# Shallow CNN for M/EEG signal classification

Tensorflow implementation of a shallow CNN with rank-1 Fourier domain spatio-temporal weights for multivariate M/EEG signal classification

![Architecture illustration](.meeg_clf_architecture.png)

# Description:
The project contains the following classes:
* . **Database** with child classes:
    *  **Database_BCI_EEG_MentalWorkload** [2]
    *  **Database_HCP_MEG_MotorTask** [3]
* . **Preprocessor** with child classes:
    *  **Preprocessor_BCI_EEG_MentalWorkload**
    *  **Preprocessor_HCP_MEG_MotorTask**
* . **Classifier** with child classes:
    *  **DeepConvNet** [4]
    *  **ShallowConvNet** [4]
    *  **EEGNet** [5]
    *  **Classifier_CNN_Rank_1** [1]

**Database** classes contain attributes for database path and list of subjects, and method for data split into training, validation and testing subsets

**Preprocessor** classes contain attributes related to the experiment setup (subject blind / subject aware), type of output (signal domain/spectral domain of the spatial component), and methods for preprocessing and data loading

**Classifier** classes contain definition of classifiers with attributes related to the input signals' number of channels, the number of sampling points, the number of convolutional kernels, the pooling sizes, etc. They contain the methods related to the models' training, validation, testing and saving.

*classifiers **DeepConvNet**, **ShallowConvNet** and **EEGNet** are adapted from : 
https://github.com/vlawhern/arl-eegmodels


# Dependencies:

* . python 3.6.13       &ensp (conda create -n shallow_rank_1_cnn python=3.6.13 & conda activate shallow_rank_1_cnn)
* . dipy                (pip install dipy)
* . matplotlib          (pip install matplotlib)
* . tensorflow 2.6.2    (pip install tensorflow==2.6.2)
* . mne-hcp             (git clone http://github.com/mne-tools/mne-hcp & python setup.py install)

Running setup.py file:
* . python setup.py develop

or
* . python setup.py install

# Stand-alone examples:

**Example 1:** model_number_parameters.ipynb
python notebook for a comparison of the number of parameters.

**Example 2:** model_number_multiplications.ipynb
python notebook for a comparison of the number of multiplications.

# Real data examples:

#### To run an experiment for mental workload EEG classification (in examples directory):

**python** **experiment_mental_workload.py** **-config_file** path_to_config_file **-db_path** path_to_database **-exp_out_path** path_for_preprocessed_data **-results_path** path_for_classifier_outputs **-seed** int

Link to dataset: https://zenodo.org/record/5055046#


#### To run an experiment for motor task MEG classification (in examples directory):

**python** **experiment_motor_task.py** **-config_file** path_to_config_file **-db_path** path_to_database **-exp_out_path** path_for_preprocessed_data **-results_path** path_for_classifier_outputs

Link to dataset: https://db.humanconnectome.org

# References:

[1] Sedlar, S., Benerradi, J., Le Breton, C., Deriche, R., Papadopoulo, T., & Wilson, M. (2021, September). Rank-1 CNN for mental workload classification from EEG. In Neuroergonomics conference.

[2] Hinss, M. F., Somon, B., Dehais, F., & Roy, R. N. (2021, May). Open EEG Datasets for Passive Brain-Computer Interface Applications: Lacks and Perspectives. In 2021 10th International IEEE/EMBS Conference on Neural Engineering (NER) (pp. 686-689). IEEE.

[3] Van Essen, D. C., Smith, S. M., Barch, D. M., Behrens, T. E., Yacoub, E., Ugurbil, K., & Wu-Minn HCP Consortium. (2013). The WU-Minn human connectome project: an overview. Neuroimage, 80, 62-79.

[4] Schirrmeister, R. T., Springenberg, J. T., Fiederer, L. D. J., Glasstetter, M., Eggensperger, K., Tangermann, M., ... & Ball, T. (2017). Deep learning with convolutional neural networks for EEG decoding and visualization. Human brain mapping, 38(11), 5391-5420.

[5] Lawhern, V. J., Solon, A. J., Waytowich, N. R., Gordon, S. M., Hung, C. P., & Lance, B. J. (2018). EEGNet: a compact convolutional neural network for EEG-based brain–computer interfaces. Journal of neural engineering, 15(5), 056013.



