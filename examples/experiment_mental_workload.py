
import os
import sys
import argparse
from shallow_cnn_meeg.load_config_file import load_config_file


def main():

    parser = argparse.\
        ArgumentParser(description='Model training, validation and testing.')

    parser.add_argument('-config_file', dest='config_file', required=True,
                        help='Configuration file.')
    parser.add_argument('-db_path', dest='db_path', required=True,
                        help='Path to the input dataset.')
    parser.add_argument('-exp_out_path', dest='exp_out_path', required=True,
                        help='Path to the experiment output.')
    parser.add_argument('-results_path', dest='results_path', required=True,
                        help='Path to the experiment results')
    parser.add_argument('-seed', dest='seed', required=True, type=int,
                        help='Path to the experiment results')

    args = parser.parse_args()

    modules_objects = load_config_file(args.config_file)

    db = modules_objects['database']
    db.set_db_path(args.db_path)
    db.get_subjects_list()
    db.split_subjects(seed=args.seed)

    prep = modules_objects['preprocessor']
    prep.preprocess_signals(db, exp_out_path=args.exp_out_path)
    prep.prepare_data(db, args.exp_out_path, seed=args.seed)

    clf = modules_objects['classifier']
    clf.train_and_valid(db, prep,
                        exp_out_path=args.exp_out_path,
                        results_path=args.results_path + '_rnd_' + str(args.seed),
                        seed=args.seed)

if __name__ == '__main__':
    main()
