"""Script for loading a configuration file."""
import sys
import json

from .database import Database
from . import databases
from .preprocessor import Preprocessor
from . import preprocessors
from .classifier import Classifier
from . import classifiers


modules =\
    {
        'database':
            {
                'mother_class': Database,
                'child_classes': databases
            },
        'preprocessor':
            {
                'mother_class': Preprocessor,
                'child_classes': preprocessors
            },
        'classifier':
            {
                'mother_class': Classifier,
                'child_classes': classifiers
            }
    }


def load_config_file(config_path):
    """Loading configuration file as dictionary."""
    """
        Arguments:
            config_path: path to the configuration file (json file)
        Returns:
            dictionary of modules' objects
    """

    try:
        with open(config_path, 'r') as f_config:
            configuration = json.load(f_config)
    except Exception as e:
        print("Invalid configuration file.")
        print("Exception:", e)
        sys.exit(2)

    if 'modules' not in configuration:
        print("Invalid configuration file.")
        sys.exit(2)

    for m in modules:
        if m not in modules:
            print("Module name <<" + m + ">> is invalid.")
            sys.exit(2)

    objects = {}
    for m in configuration['modules']:
        mother_class = modules[m]['mother_class']
        youngest_classes = modules[m]['child_classes']
        if 'grandchild_classes' in modules[m]:
            youngest_classes_list = modules[m]['grandchild_classes']
            for youngest_classes in youngest_classes_list:
                if configuration['modules'][m]['name'] in dir(youngest_classes):
                    break

        module_class = getattr(youngest_classes, configuration['modules'][m]['name'])

        #if not issubclass(module_class, mother_class):
        #    print("Class " + youngest_classes.__name__ + " is not a (grand)child class of " + mother_class.__name__)
        #    sys.exit(2)

        module_obj = module_class(**configuration['modules'][m]['arguments'])
        objects[m] = module_obj

    return objects
