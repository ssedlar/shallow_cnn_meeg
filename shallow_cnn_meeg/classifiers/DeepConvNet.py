
import os
import time
import numpy as np
import tensorflow as tf

from .. import Classifier

from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import BatchNormalization, Dropout
from tensorflow.keras.layers import Dense, Activation, Flatten

from tensorflow.keras.constraints import max_norm


class DeepConvNet(Classifier):
    def __init__(self, Chans = 61, Samples = 167,
                 dropoutRate = 0.5, 
                 n_conv_k=12, len_conv_k=5, max_pool_step=2, 
                 norm_n=2.0, norm_n_fcn=0.5, **kwargs):
        super(DeepConvNet, self).__init__(**kwargs)

        ######################################################################
        self.n_conv_k = n_conv_k
        self.len_conv_k = len_conv_k
        self.max_pool_step = max_pool_step
        self.norm_n = norm_n
        self.norm_n_fcn = norm_n_fcn
        ######################################################################
        self.conv0_1 = Conv2D(self.n_conv_k, (1, self.len_conv_k), 
                            input_shape=(Chans, Samples, 1),
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0, 1, 2)))
        self.conv0_2 = Conv2D(self.n_conv_k, (Chans, 1),
                              kernel_constraint=\
                              max_norm(self.norm_n, axis=(0, 1, 2)))
        self.bn0 = BatchNormalization(epsilon=1e-05, momentum=0.9)
        self.act0 = Activation('elu')
        self.max_pool0 = MaxPooling2D(pool_size=(1, self.max_pool_step), 
                                      strides=(1, self.max_pool_step))
        self.dp0 = Dropout(dropoutRate)
        ######################################################################
        self.conv1 = Conv2D(2 * self.n_conv_k, (1, self.len_conv_k),
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0,1,2)))
        self.bn1 = BatchNormalization(epsilon=1e-05, momentum=0.9)
        self.act1 = Activation('elu')
        self.max_pool1 = MaxPooling2D(pool_size=(1, self.max_pool_step), 
                                      strides=(1, self.max_pool_step))
        self.dp1 = Dropout(dropoutRate)
        ######################################################################
        self.conv2 = Conv2D(4 * self.n_conv_k, (1, self.len_conv_k),
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0,1,2)))
        self.bn2 = BatchNormalization(epsilon=1e-05, momentum=0.9)
        self.act2 = Activation('elu')
        self.max_pool2 = MaxPooling2D(pool_size=(1, self.max_pool_step), 
                                      strides=(1, self.max_pool_step))
        self.dp2 = Dropout(dropoutRate)
        ######################################################################
        self.conv3 = Conv2D(8 * self.n_conv_k, (1, self.len_conv_k),
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0,1,2)))
        self.bn3 = BatchNormalization(epsilon=1e-05, momentum=0.9)
        self.act3 = Activation('elu')
        self.max_pool3 = MaxPooling2D(pool_size=(1, self.max_pool_step), 
                                      strides=(1, self.max_pool_step))
        self.dp3 = Dropout(dropoutRate)
        ######################################################################
        self.flatten = Flatten()
        self.dense = Dense(self.n_classes,
                           kernel_constraint=max_norm(self.norm_n_fcn))
        self.softmax = Activation('softmax')
        ######################################################################
        self.loss = tf.keras.losses.CategoricalCrossentropy()

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.lr)

    def __call__(self, input, training=None):

        input = tf.expand_dims(input, axis=3)

        # 1. First convolutional layer with separable convolutions
        ######################################################################
        x0_sc = self.conv0_2(self.conv0_1(input))
        x0 = self.dp0(self.max_pool0(self.act0(self.bn0(x0_sc, 
                                                        training=training))), 
                      training=training)
        ######################################################################

        # 2. Second convolutional layer with standard convolutions
        ######################################################################
        x1_c = self.conv1(x0)
        x1 = self.dp1(self.max_pool1(self.act1(self.bn1(x1_c, 
                                                        training=training))), 
                      training=training)
        ######################################################################

        # 3. Third convolutional layer with standard convolutions
        ######################################################################
        x2_c = self.conv2(x1)
        x2 = self.dp2(self.max_pool2(self.act2(self.bn2(x2_c, 
                                                        training=training))), 
                      training=training)
        ######################################################################

        # 4. Fourth convolutional layer with standard convolutions
        ######################################################################
        x3_c = self.conv3(x2)
        x3 = self.dp3(self.max_pool3(self.act3(self.bn3(x3_c, 
                                                        training=training))), 
                      training=training)
        ######################################################################
        # 5. Fully connected layer
        ######################################################################
        x4 = self.softmax(self.dense(self.flatten(x3)))
        ######################################################################
        return x4

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(name=%s, n_conv_k=%s, len_conv_k=%s, " \
               "max_pool_step=%s, lr=%s)" % \
                (type(self).__name__, self.exp_name,  self.n_conv_k, 
                 self.len_conv_k, self.max_pool_step, self.lr)