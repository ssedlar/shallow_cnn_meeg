
import os
import time
import numpy as np
import tensorflow as tf

from .. import Classifier

from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import BatchNormalization, Dropout
from tensorflow.keras.layers import Dense, Activation, Flatten

from tensorflow.keras.constraints import max_norm
from tensorflow.keras import backend as K


def square(x):
    return K.square(x)


def log(x):
    return K.log(K.clip(x, min_value = 1e-7, max_value=10000))


class ShallowConvNet(Classifier):
    def __init__(self, Chans = 61, Samples = 167,
                 dropoutRate = 0.5,
                 n_conv_k=12, len_conv_k=5, max_pool_step=2, 
                 norm_n=2.0, norm_n_fcn=0.5, **kwargs):
        super(ShallowConvNet, self).__init__(**kwargs)
        ######################################################################
        self.n_conv_k = n_conv_k
        self.len_conv_k = len_conv_k
        self.max_pool_step = max_pool_step
        self.norm_n = norm_n
        self.norm_n_fcn = norm_n_fcn
        ######################################################################
        self.conv0 = Conv2D(self.n_conv_k, (1, self.len_conv_k),
                            input_shape=(Chans, Samples, 1),
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0, 1, 2)))
        self.conv1 = Conv2D(self.n_conv_k, (Chans, 1), use_bias=False,
                            kernel_constraint=\
                            max_norm(self.norm_n, axis=(0, 1, 2)))
        self.bn0 = BatchNormalization(epsilon=1e-05, momentum=0.9)
        self.act0 = Activation(square)
        self.avg_pool0 = AveragePooling2D(pool_size=(1, self.max_pool_step), 
                                          strides=(1, self.max_pool_step//5))
        self.act1 = Activation(log)
        self.dp0 = Dropout(dropoutRate)
        ######################################################################
        self.flatten = Flatten()
        self.dense = Dense(self.n_classes, 
                           kernel_constraint=max_norm(self.norm_n_fcn))
        self.softmax = Activation('softmax')
        ######################################################################
        self.loss = tf.keras.losses.CategoricalCrossentropy()
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.lr)

    def __call__(self, input, training=None):

        input = tf.expand_dims(input, axis=3)

        # 1. First convolutional layer with separable convolutions
        ######################################################################
        x0_sc = self.conv1(self.conv0(input))
        x0_sc_bn = self.bn0(x0_sc, training=training)
        x0 = self.dp0(self.act1(self.avg_pool0(self.act0(x0_sc_bn))), 
                      training=training)
        ######################################################################

        # 2. Fully connected layer
        ######################################################################
        x1 = self.softmax(self.dense(self.flatten(x0)))
        ######################################################################
        return x1

    @property
    def name(self):
        """Return object's name including parameters."""
        return ("%s(name=%s, n_conv_k=%s, len_conv_k=%s, "+\
                "max_pool_step=%s, lr=%s)" % 
                (type(self).__name__, self.exp_name, 
                 self.n_conv_k, self.len_conv_k, self.max_pool_step, self.lr))