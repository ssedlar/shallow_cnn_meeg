
import os
import time
import numpy as np
import tensorflow as tf

from .. import Classifier

from tensorflow.keras.layers import Conv2D, SeparableConv2D
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Dropout,SpatialDropout2D
from tensorflow.keras.layers import Dense, Activation, Flatten

from tensorflow.keras.constraints import max_norm

class EEGNet(Classifier):
    """ Keras Implementation of EEGNet

    DOCUMENT SOURCE: https://github.com/vlawhern/arl-eegmodels/blob/master/EEGModels.py

    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta
    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
    advised to do some model searching to get optimal performance on your
    particular dataset.
    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 
    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.
    """
    def __init__(self, Chans = 61, Samples = 167, 
                 dropoutRate = 0.5, kernLength = 340, F1 = 16, 
                 D = 2, F2 = 32, norm_n=1.0, norm_n_fcn = 0.25, 
                 dp_type = 'Dropout', avg_p1=4, avg_p2=8, **kwargs):
        super(EEGNet, self).__init__(**kwargs)

        self.kernLength = kernLength
        self.dp_type = dp_type
        self.avg_p1 = avg_p1
        self.avg_p2 = avg_p2

        self.F1 = F1
        self.F2 = 2 * F1

        self.norm_n = norm_n
        self.norm_n_fcn = norm_n_fcn

        ######################################################################
        if self.dp_type  == 'SpatialDropout2D':
            dropoutType = SpatialDropout2D
        elif self.dp_type  == 'Dropout':
            dropoutType = Dropout
        else:
            raise ValueError('dropoutType must be one of SpatialDropout2D '
                             'or Dropout, passed as a string.')
    
        ######################################################################
        self.conv0 = Conv2D(self.F1, (1, self.kernLength), padding = 'same',
                            input_shape = (Chans, Samples, 1),
                            use_bias = False)
        self.bn0 = BatchNormalization()
        ######################################################################
        self.depth_conv1 = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                           depth_multiplier = D,
                                           depthwise_constraint=\
                                           max_norm(self.norm_n))
        self.bn1 = BatchNormalization()
        self.act1 = Activation('elu')
        self.avg_pool1 = AveragePooling2D((1, self.avg_p1))
        self.dp1 = dropoutType(dropoutRate)
        ######################################################################
        self.sep_conv2 = SeparableConv2D(self.F2, (1, 16),
                                         use_bias = False, padding = 'same')
        self.bn2 = BatchNormalization()
        self.act2 = Activation('elu')
        self.avg_pool2 = AveragePooling2D((1, self.avg_p2))
        self.dp2  = dropoutType(dropoutRate)
        ######################################################################
        self.flatten = Flatten(name = 'flatten')
        self.dense = Dense(self.n_classes, name = 'dense', 
                           kernel_constraint = max_norm(self.norm_n_fcn))
        self.softmax = Activation('softmax', name = 'softmax')
        ######################################################################
        self.loss = tf.keras.losses.CategoricalCrossentropy()
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.lr)

    def __call__(self, input, training=None):

        input = tf.expand_dims(input, axis=3)

        # 1. First temporal convolutional layer convolutions
        ######################################################################
        x0_c = self.conv0(input)
        x0 = self.bn0(x0_c, training=training)
        ######################################################################

        # 2. First spatial convolutional layer with depthwise convolutions
        ######################################################################
        x1_c = self.depth_conv1(x0)
        x1 = self.dp1(self.avg_pool1(self.act1(self.bn1(x1_c, 
                                                        training=training))), 
                      training=training)
        ######################################################################

        # 3. Second separable convolutional layer
        ######################################################################
        x2_sc = self.sep_conv2(x1)
        x2 = self.dp2(self.avg_pool2(self.act2(self.bn2(x2_sc, 
                                                        training=training))), 
                      training=training)
        ######################################################################

        # 4. Fully connected layer
        ######################################################################
        x3 = self.softmax(self.dense(self.flatten(x2)))
        ######################################################################
        return x3

    @property
    def name(self):
        """Return object's name including parameters."""
        return ("%s(name=%s, kernLength=%s, dp_type=%s, "+\
                "avg_p1=%s, avg_p2=%s, norm_rate=%s, F1=%s, lr=%s)" % 
                (type(self).__name__, self.exp_name, self.kernLength, 
                 self.dp_type, self.avg_p1, self.avg_p2, 
                 self.norm_rate, self.F1, self.lr))