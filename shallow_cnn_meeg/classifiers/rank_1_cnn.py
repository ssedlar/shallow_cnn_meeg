
import os
import numpy as np
import tensorflow as tf

from .. import Classifier

from tensorflow.keras.layers import Dense, Flatten, BatchNormalization, Dropout
from tensorflow.keras.constraints import max_norm

from .utils import dct_basis


class Classifier_CNN_Rank_1(Classifier):

    def __init__(self, 
                 spat_L=12, temp_T=42, temp_B=41, n_st_k=10, pool=2, **kwargs):
        super(Classifier_CNN_Rank_1, self).__init__(**kwargs)

        dct = dct_basis(n_samples=temp_T, max_freq=temp_B-1)
        self.dct = tf.convert_to_tensor(dct, dtype=tf.float32)

        self.spat_L = spat_L
        self.spat_n = (self.spat_L + 1) ** 2
        self.temp_T = temp_T
        self.temp_B = temp_B
        self.n_st_k = n_st_k

        self.pool = pool

        init = tf.keras.initializers.GlorotNormal()
        self.w_S = tf.Variable(init(shape=(self.spat_n, self.n_st_k)), 
                               trainable=True, 
                               constraint=max_norm(0.5, axis=0))
        self.w_T = tf.Variable(init(shape=(self.temp_B, self.n_st_k)), 
                               trainable=True, 
                               constraint=max_norm(0.5, axis=0))
        self.b_T = tf.Variable(np.zeros(self.n_st_k).astype(np.float32), 
                               trainable=True)
        self.bn_0 = BatchNormalization(name='bn_0', center=False, scale=False,
                                       moving_mean_initializer='zeros',
                                       moving_variance_initializer='zeros')
        self.dp_0 = Dropout(0.5)
        self.flatten = Flatten()
        self.fcn1 = Dense(self.n_classes, activation='softmax', 
                          kernel_initializer=init, 
                          kernel_constraint=max_norm(0.5, axis=0))

        self.loss = tf.keras.losses.CategoricalCrossentropy()

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.lr)

    def __call__(self, input, training=None):

        input_r = tf.gather(input, tf.range(0, self.spat_n), axis=1)
        input_r = tf.transpose(input_r, (0, 2, 1))

        input_spat = tf.einsum('bts,so->bto', input_r, self.w_S) 
        input_spat = tf.expand_dims(input_spat, axis=1)
        kernels_t = tf.einsum('tf, fn->tn', self.dct, self.w_T)
        kernels_t = tf.expand_dims(kernels_t, axis=0)
        kernels_t = tf.expand_dims(kernels_t, axis=3)

        z = tf.nn.depthwise_conv2d(input_spat, kernels_t, 
                                   padding='VALID', strides=[1, 1, 1, 1])
        zb = z + self.b_T
        zb = tf.squeeze(zb, axis=1)
        a = tf.nn.relu(zb)
        s = tf.keras.layers.MaxPool1D(pool_size=self.pool)(a)
        shat = self.bn_0(s, training=training)
        shat_dp = self.dp_0(shat, training=training)
        shat_flat = self.flatten(shat_dp)

        c = self.fcn1(shat_flat)

        return c

    @property
    def name(self):
        """Return object's name including parameters."""
        return ("%s(name=%s, spat_L=%s, temp_B=%s, n_st_k=%s, pool=%s, lr=%s)" %
                (type(self).__name__, self.exp_name, self.spat_L, self.temp_B, 
                 self.n_st_k, self.pool, self.lr))