
from .rank_1_cnn import Classifier_CNN_Rank_1
from .EEGNet import EEGNet
from .ShallowCovNet import ShallowConvNet
from .DeepConvNet import DeepConvNet
