
import numpy as np

def dct_basis(n_samples, max_freq):

    t_points = np.arange(0, n_samples)

    dct = np.zeros((t_points.shape[0], max_freq + 1))
    for i in range(max_freq + 1):
        dct[:, i] = np.cos(i*(t_points + 1 / 2)* np.pi/n_samples)
    dct[:, 0] /= np.sqrt(t_points.shape[0])
    dct[:, 1:] /= (np.sqrt(t_points.shape[0] / 2))

    return dct