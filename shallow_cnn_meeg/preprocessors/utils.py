import numpy as np
from scipy.special import sph_harm
from dipy.core.geometry import cart2sphere, sphere2cart

def convert_cart_to_s2(points_cart):
    """Coverts array of points from Cartesian to S2 coordinate system"""
    """
        Arguments:
            points_cart (numpy array N x 3): array of points in Cartesian 
                coordinate system
        Returns:
            numpy array N x 2: array of points in S2 coordinate system
                (where r=1)
    """
    points_s2 = np.zeros((points_cart.shape[0], 2))
    for i in range(points_cart.shape[0]):
        x, y, z = points_cart[i, :]
        r, theta, phi = cart2sphere(x, y, z)
        points_s2[i, 0] = theta
        points_s2[i, 1] = phi

    return points_s2


def sh_basis_real(s2_coord, L, even=False):
    """Real spherical harmonic basis of even degree."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
        Returns:
            numpy array: spherical harmonic bases (each column is a basis)
    """
    s = 2 if even else 1
    n_sph = np.sum([2 * i + 1 for i in range(0, L + 1, s)])
    Y = np.zeros((s2_coord.shape[0], n_sph), dtype=np.float32)
    n_sph = 0
    for i in range(0, L + 1, s):
        ns, ms = np.zeros(2 * i + 1) + i, np.arange(-i, i + 1)
        Y_n_m = sph_harm(ms, ns, s2_coord[:, 1:2], s2_coord[:, 0:1])
        if i > 0:
            Y_n_m[:, 0:i] = np.sqrt(2) * \
                np.power(-1, np.arange(0, i)) * np.imag(Y_n_m[:, :i:-1])
            Y_n_m[:, (i + 1):] = np.sqrt(2) * \
                np.power(-1, np.arange(1, i + 1)) * np.real(Y_n_m[:, (i + 1):])
        Y[:, n_sph:n_sph + 2 * i + 1] = Y_n_m
        n_sph += 2 * i + 1
    return Y

def lms_laplace_beltrami_sh_inv(s2_coord, L, lambda_):
    """Inversion of spherical harmonic basis with least-mean square
       requilarized with Laplace-Beltrami regularization term."""

    """
        References:
        [1] Descoteaux, Maxime, et al.
        "Regularized, fast, and robust analytical Q-ball imaging."
        Magnetic Resonance in Medicine: An Official Journal of the
        International Society for Magnetic Resonance in Medicine 58.3 (2007):
        497-510.

        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
            lambda_ (float): regularization weight 
        Returns:
            numpy array: inverted spherical harmonic bases 
    """
    Y = sh_basis_real(s2_coord, L)

    lb_reg = np.zeros((Y.shape[1], Y.shape[1]), dtype=np.float32)
    count = 0
    for l in range(0, L + 1):
        for m in range(-l, l + 1):
            lb_reg[count, count] = l ** 2 * (l + 1) ** 2
            count += 1

    return np.dot(np.linalg.inv(np.dot(Y.T, Y) + lambda_ * lb_reg), Y.T)