
import os
import numpy as np
from scipy.special import sph_harm
from dipy.core.geometry import cart2sphere, sphere2cart


from .. import Preprocessor
import mne
from mne.io.pick import pick_types
from mne.bem import _fit_sphere
import matplotlib.pyplot as plt

from .utils import convert_cart_to_s2
from .utils import lms_laplace_beltrami_sh_inv

tmin, tmax = -1.2, 1.2
baseline = (-0.3, 0)

REBUILD = True
N_SUBJ = 15
DIFFICULTIES = ['MATBeasy', 'MATBmed', 'MATBdiff']
DS_FACTOR = 3
N_EPOCHS = 25
BATCH_SIZE = 128


class Preprocessor_BCI_EEG_MentalWorkload(Preprocessor):

    def __init__(self, n_spat=61, n_temp=167,
                 subj_aware=False, spat_type='sph', L=None, lambda_=None):

        self.n_spat = n_spat
        self.n_temp = n_temp

        self.subj_aware = subj_aware
        self.spat_type = spat_type

        self.L = L
        self.lambda_ = lambda_
        if self.spat_type == 'sph':
            if L == None or lambda_ == None:
                raise("For spherical harmonic type of spatial dimension"+\
                      "L and lambda_ parameters must be specified")
        else:
            self.L = None
            self.lambda_ = None

    def preprocess_signals(self, db, exp_out_path):

        if os.path.exists(os.path.join(exp_out_path, 'done')):
            return
        '''
        channel_positions_path = os.path.join(db.db_path, 'chan_locs_standard')
        channel_positions = {}
        with open(channel_positions_path) as f:
            channel_positions_str = f.readlines()
            for ch in channel_positions_str:
                ch_split = ch.strip().split()
                channel_positions[ch_split[0]] = [float(ch_split[1]), float(ch_split[2]), float(ch_split[3])]

        print(channel_positions)
        '''
        ######################################################################
        # 2. Iterate trough list of subjects and preprocess data
        ######################################################################
        for subj_idx, s in enumerate(db.subjects_list):
            ##################################################################
            # 2.1. Iterate trough sessions
            ##################################################################
            for sess_idx, sess in enumerate([1, 2]):
                ##############################################################
                # 2.1.1. Iterate trough difficulties
                ##############################################################
                for d_idx, d in enumerate(['MATBeasy', 'MATBmed', 'MATBdiff']):
                    if int(s) < 10:
                        st = '0' + str(s)
                    else:
                        st = str(s)
                    ##########################################################
                    # 2.1.1.1. Define output path 
                    #          for one subject, one session and one event
                    ##########################################################
                    pp_out_path = os.path.join(exp_out_path,
                                               'subject_' + s,
                                               'session_' + str(sess_idx),
                                               'event_' + str(d_idx))
                    if not os.path.exists(pp_out_path):
                        os.makedirs(pp_out_path)
                    ##########################################################
                    # 2.1.1.2. Epoch raw signals given event id
                    ##########################################################
                    ep_path = \
                        os.path.join(db.db_path, 'P{0}', 'S{1}', 
                                     'eeg', 'alldata_sbj{0}_sess{1}_{2}.set')
                    epochs = \
                        mne.io.read_epochs_eeglab(ep_path.format(st, sess, d), 
                                                  verbose=False)
                    ##########################################################
                    # 2.1.1.3. Downsample epochs
                    #          (after bandpass/lowpass filtering!)
                    ##########################################################
                    data_epochs = epochs.get_data()[:, :, ::3]
                    epochs_path = os.path.join(pp_out_path, 'data.npy')
                    ##########################################################
                    # 2.1.1.4. Extract channel coordinates 
                    #          and fit them to a sphere
                    ##########################################################
                    picks = pick_types(epochs.info, eeg=True, exclude=[])
                    '''
                    channel_names = epochs.ch_names
                    pos_eeg = np.zeros((61, 3))
                    for ch_idx, ch in enumerate(channel_names):
                        pos_eeg[ch_idx, :] = channel_positions[ch]
                    '''
                    pos_eeg = epochs._get_channel_positions(picks)
                    channel_names = epochs.ch_names
                    if (subj_idx == 2 and sess_idx == 0) or \
                       (subj_idx == 3 and sess_idx == 0) or \
                       (subj_idx == 5 and sess_idx == 0) or \
                       (subj_idx == 6) :
                        pos_eeg[:, 1:] *= -1
                        epochs._set_channel_positions(pos_eeg, channel_names)

                    radius, center = _fit_sphere(pos_eeg)
                    distance = np.sqrt(np.sum((pos_eeg - center) ** 2, 1))
                    distance = np.mean(distance / radius)
                    if np.abs(1. - distance) > 0.1:
                        print('Your spherical fit is poor.')

                    pos_eeg_c = pos_eeg - center
                    pos_eeg_c_n = np.sqrt(np.sum(pos_eeg_c ** 2, axis=1))
                    pos_eeg_c /= np.expand_dims(pos_eeg_c_n, axis=1)

                    c_n_path = os.path.join(pp_out_path, 'norm_coords.npy')
                    c_o_path = os.path.join(pp_out_path, 'orig_coords.npy')
                    data_info_path = os.path.join(pp_out_path, 'data_info')

                    np.save(epochs_path, data_epochs * 1.e4)
                    np.save(c_o_path, pos_eeg)
                    np.save(c_n_path, pos_eeg_c)
                    with open(data_info_path, 'a') as f:
                        f.write("%d %d %d" % (data_epochs.shape[0], 
                                              data_epochs.shape[1], 
                                              data_epochs.shape[2]))
                    ##########################################################

    def prepare_data(self, db, exp_out_path, seed):
        ######################################################################
        # 1. Define training data path
        ######################################################################
        train_data_path = \
            os.path.join(exp_out_path, 
                         'train(subj_aware={0}, {1}, L={2}, lambda_={3}, seed={4})'.\
                         format(self.subj_aware, self.spat_type, 
                                self.L, self.lambda_, seed))
        if os.path.exists(os.path.join(train_data_path, 'done')):
            return
        ######################################################################
        # 2. Define training subject and session lists
        ######################################################################
        if self.subj_aware:
            sess = np.concatenate((np.zeros(db.n_train, dtype=np.int8),
                                   np.ones(db.n_train, dtype=np.int8),
                                   np.zeros(db.n_valid, dtype=np.int8),
                                   np.zeros(db.n_test, dtype=np.int8)))
            subjs = db.train_list + db.train_list + \
                db.valid_list + db.test_list
        else:
            sess = np.concatenate((np.zeros(db.n_train, dtype=np.int8),
                                   np.ones(db.n_train, dtype=np.int8)))
            subjs = db.train_list + db.train_list
        ######################################################################
        # 3. Iterate trough events and load data for created lists of
        #    subjects and sessions
        ######################################################################
        for ev_idx, ev in enumerate([0, 1, 2]):
            t_data, t_gt = self.load_data(exp_out_path, subjs, sess, [ev])
            t_data_ev_path = os.path.join(train_data_path, 'event_' + str(ev))
            if not os.path.exists(t_data_ev_path):
                os.makedirs(t_data_ev_path)
            np.save(os.path.join(t_data_ev_path, 'train_data.npy'), t_data)
        ######################################################################
        # 4. Define validation data path
        ######################################################################
        valid_data_path = \
            os.path.join(exp_out_path, 
                         'valid(subj_aware={0}, {1}, L={2}, lambda_={3}, seed={4})'.\
                            format(self.subj_aware, self.spat_type, 
                                   self.L, self.lambda_, seed))
        ######################################################################
        # 5. Define validation subject and session lists
        ######################################################################
        if self.subj_aware:
            sess = np.ones(db.n_valid,  dtype=np.int8)
            subjs = db.valid_list
        else:
            sess = np.concatenate((np.zeros(db.n_valid, dtype=np.int8),
                                   np.ones(db.n_valid, dtype=np.int8)))
            subjs = db.valid_list + db.valid_list
        ######################################################################
        # 6. Iterate trough events and load data for created lists of
        #    subjects and sessions
        ######################################################################
        for ev_idx, ev in enumerate([0, 1, 2]):
            v_data, v_gt = self.load_data(exp_out_path, subjs, sess, [ev])
            v_data_ev_path = os.path.join(valid_data_path, 'event_' + str(ev))
            if not os.path.exists(v_data_ev_path):
                os.makedirs(v_data_ev_path)
            np.save(os.path.join(v_data_ev_path, 'valid_data.npy'), v_data)
        ######################################################################
        # 7. Set up done flag
        ######################################################################
        with open(os.path.join(train_data_path, 'done'), 'a+') as f:
            f.close()

    def load_prepared_data(self, db, exp_out_path, subset='train', seed=1):
        """Loads previously prepared merged data."""
        ######################################################################
        # 1. Define data path
        ######################################################################
        data_path = \
            os.path.join(exp_out_path, 
                         '{0}(subj_aware={1}, {2}, L={3}, lambda_={4}, seed={5})'.\
                         format(subset, self.subj_aware, self.spat_type, 
                                self.L, self.lambda_, seed))
        ######################################################################
        # 2. Count number of trials for memory allocation
        ######################################################################
        c = 0
        for ev_idx, ev in enumerate([0, 1, 2]):
            ev_path = os.path.join(data_path, 'event_{0}'.format(ev))
            ev_data = np.load(os.path.join(ev_path, subset + '_data.npy'))
            c += ev_data.shape[0]
            n = ev_data.shape[1]
            t = ev_data.shape[2]
        ######################################################################
        # 3. Load prepared data
        ######################################################################
        data = np.zeros((c, n, t), dtype=np.float32)
        gt = np.zeros((c, 3), dtype=np.int8)
        c = 0
        for ev_idx, ev in enumerate([0, 1, 2]):
            ev_path = os.path.join(data_path, 'event_{0}'.format(ev))
            ev_data = np.load(os.path.join(ev_path, subset+'_data.npy'))
            n_ev = ev_data.shape[0]
            data[c:c + n_ev, :, :] = ev_data
            gt[c:c + n_ev, ev_idx] = 1
            c += n_ev
        ######################################################################
        return data, gt

    def load_data(self, exp_out_path, subjects, sessions, evs=[0, 1, 2]):
        ######################################################################
        # 1. Count number of trials for memory allocation
        ######################################################################
        n_subjects = len(subjects)
        c = 0
        for s_idx in range(n_subjects):
            for ev in evs:
                pp_out_path = os.path.join(exp_out_path,
                                           'subject_' + subjects[s_idx],
                                           'session_' + str(sessions[s_idx]),
                                           'event_' + str(ev))
                info_path = os.path.join(pp_out_path, 'data_info')
                with open(info_path, 'r') as f_inf:
                    line = str.split(f_inf.readline())
                    n = int(line[1])
                    c += int(line[0])
        ######################################################################
        # 2. Load preprocessed data
        ######################################################################
        if self.spat_type == 'sph':
            n = np.sum([2 * l + 1 for l in range(0, self.L+1)])
        data = np.zeros((c, n, self.n_temp), dtype=np.float32)
        gt = np.zeros((c, 3), dtype=np.int8)
        c = 0
        for s_idx in range(n_subjects):
            for ev_idx, ev in enumerate(evs):
                pp_out_path = os.path.join(exp_out_path,
                                           'subject_' + subjects[s_idx],
                                           'session_' + str(sessions[s_idx]),
                                           'event_' + str(ev))

                data_s = np.load(os.path.join(pp_out_path, 'data.npy')) * 5
                if self.spat_type == 'sph':
                    coords_path = os.path.join(pp_out_path, 'norm_coords.npy')
                    coords_cart = np.load(coords_path)
                    coords_s2 = convert_cart_to_s2(coords_cart)
                    Y_inv = lms_laplace_beltrami_sh_inv(coords_s2, 
                                                        L=self.L, 
                                                        lambda_=self.lambda_)
                    data_s = np.einsum('lp,npt->nlt', Y_inv, data_s)

                n_trials = data_s.shape[0]
                gt[c:c + n_trials, ev_idx] = 1
                data[c:c + n_trials, :, :] = data_s[:, :, :]
                c += n_trials
        ######################################################################
        return data, gt

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
