
import os
import numpy as np
from scipy.special import sph_harm
from dipy.core.geometry import cart2sphere, sphere2cart


from .. import Preprocessor
import hcp
import mne
import hcp.preprocessing as preproc
from mne.io.pick import pick_types
from mne.bem import _fit_sphere

from .utils import convert_cart_to_s2
from .utils import lms_laplace_beltrami_sh_inv

tmin, tmax = -1.2, 1.2
baseline = (-0.3, 0)

class Preprocessor_HCP_MEG_MotorTask(Preprocessor):

    def __init__(self, n_spat=248, n_temp=407, 
                 subj_aware=1, spat_type='sph', L=None, lambda_=None):

        self.n_spat = n_spat
        self.n_temp = n_temp

        self.subj_aware = subj_aware
        self.spat_type = spat_type

        self.L = L
        self.lambda_ = lambda_
        if self.spat_type == 'sph':
            if L == None or lambda_ == None:
                raise("For spherical harmonic type of spatial dimension"+\
                      "L and lambda_ parameters must be specified")
        else:
            self.L = None
            self.lambda_ = None


    def preprocess_signals(self, db, exp_out_path):
        ######################################################################
        # 1. Check if data is already preprocessed
        ######################################################################
        if os.path.exists(os.path.join(exp_out_path, 'done')):
            return
        ######################################################################
        # 2. Iterate trough list of subjects and preprocess data
        ######################################################################
        for subj_idx, s in enumerate(db.subjects_list):
            print("Subj idx:", subj_idx)
            print("\n")
            ##################################################################
            # 2.1. Loading subject's parameters
            ##################################################################
            try:
                hcp_subj_params = dict(hcp_path=db.db_path, subject=s,
                                       data_type='task_motor')
                trial_infos = list()
                for r_idx in [0, 1]:
                    hcp_subj_params['run_index'] = r_idx
                    trial_info = hcp.read_trial_info(**hcp_subj_params)
                    trial_infos.append(trial_info)

                all_events = list()
                for trial_info in trial_infos:
                    events = np.c_[
                        trial_info['stim']['codes'][:, 3] - 1,
                        np.zeros(len(trial_info['stim']['codes'])),
                        trial_info['stim']['codes'][:, 1] 
                    ].astype(int)

                    unq_subset = np.nonzero(np.r_[1, np.diff(events[:, 0])])
                    events = events[unq_subset[0]] 
                    all_events.append(events)
            except:
                print("No motor task for subject:", s)
                continue
            ##################################################################
            # 2.2. Iterate trough sessions
            ##################################################################
            for r_idx, events in zip([0, 1], all_events):
                ##############################################################
                # 2.2.1. Load raw data for one session
                ##############################################################
                hcp_subj_params = dict(hcp_path=db.db_path, 
                                       data_type='task_motor', 
                                       subject=s, run_index=r_idx)
                raw = hcp.read_raw(**hcp_subj_params)
                raw.load_data()
                ##############################################################
                # 2.2.2. Apply preprocessing of raw data
                #        - reference correction
                #        - band pass filtering
                #        - ICA
                ##############################################################
                preproc.apply_ref_correction(raw)
                annots = hcp.read_annot(**hcp_subj_params)
                # construct MNE annotations
                bad_seg = (annots['segments']['all']) / raw.info['sfreq']
                annotations = mne.Annotations(
                    bad_seg[:, 0], (bad_seg[:, 1] - bad_seg[:, 0]),
                    description='bad')
                raw.set_annotations(annotations)
                raw.info['bads'].extend(annots['channels']['all'])
                raw.pick_types(meg=True, ref_meg=False)

                raw.filter(0.50, None, method='iir',
                           iir_params=dict(order=4, ftype='butter'), n_jobs=1)
                raw.filter(None, 60, method='iir',
                           iir_params=dict(order=4, ftype='butter'), n_jobs=1)

                # read ICA and remove EOG ECG
                # note that the HCP ICA assumes that bad channels have already 
                # been removed
                ica_mat = hcp.read_ica(**hcp_subj_params)

                # We will select the brain ICs only
                ex = [ii for ii in range(annots['ica']['total_ic_number'][0])
                           if ii not in annots['ica']['brain_ic_vs']]
                preproc.apply_ica_hcp(raw, ica_mat=ica_mat, exclude=ex)

                ##############################################################
                # 2.2.3. Iterate trough events
                ##############################################################
                for ev_id in [1, 2, 4, 5, 6]:
                    ##########################################################
                    # 2.2.3.1. Define output path 
                    #          for one subject, one session and one event
                    ##########################################################
                    pp_out_path = os.path.join(exp_out_path,
                                               'subject_' + s,
                                               'session_' + str(r_),
                                               'event_' + str(ev_id))
                    if not os.path.exists(pp_out_path):
                        os.makedirs(pp_out_path)
                    ##########################################################
                    # 2.2.3.2. Epoch raw signals given event id
                    ##########################################################
                    epochs = \
                        mne.Epochs(raw, event_id=ev_id, 
                                   events=events[events[:, 2] == ev_id],
                                   tmin=tmin, tmax=tmax, baseline=baseline,
                                   reject=None, decim=1, preload=True)
                    epochs = \
                        preproc.interpolate_missing(epochs, mode='accurate', 
                                                    **hcp_subj_params)

                    ##########################################################
                    # 2.2.3.3. Downsample epochs
                    #          (after bandpass/lowpass filtering!)
                    ##########################################################
                    data_epochs = epochs.get_data()[:, :, ::12]
                    epochs_path = os.path.join(pp_out_path, 'data.npy')
                    ##########################################################
                    # 2.2.3.4. Extract channel coordinates 
                    #          and fit them to a sphere
                    ##########################################################
                    picks = pick_types(epochs.info, meg=True, eeg=False, 
                                       exclude=[])
                    pos_meg = epochs._get_channel_positions(picks)
                    radius, center = _fit_sphere(pos_meg)
                    distance = np.sqrt(np.sum((pos_meg - center) ** 2, 1))
                    distance = np.mean(distance / radius)
                    if np.abs(1. - distance) > 0.1:
                        print('Your spherical fit is poor.')

                    pos_meg_c = pos_meg - center
                    pos_meg_c_n = np.sqrt(np.sum(pos_meg_c ** 2, axis=1))
                    pos_meg_c /= np.expand_dims(pos_meg_c_n, axis=1)

                    c_n_path = os.path.join(pp_out_path, 'norm_coords.npy')
                    c_o_path = os.path.join(pp_out_path, 'orig_coords.npy')

                    np.save(epochs_path, data_epochs * 1.e12)
                    np.save(c_o_path, pos_meg)
                    np.save(c_n_path, pos_meg_c)
                    ##########################################################

    def prepare_data(self, db, exp_out_path):
        ######################################################################
        # 1. Define training data path
        ######################################################################
        train_data_path = \
            os.path.join(exp_out_path, 
                         'train(subj_aware={0}, {1}, L={2}, lambda_={3})'.\
                         format(self.subj_aware, self.spat_type, 
                                self.L, self.lambda_))
        if os.path.exists(os.path.join(train_data_path, 'done')):
            return
        ######################################################################
        # 2. Define training subject and session lists
        ######################################################################
        if self.subj_aware:
            sess = np.concatenate((np.zeros(db.n_train, dtype=np.int8),
                                   np.ones(db.n_train, dtype=np.int8),
                                   np.zeros(db.n_valid, dtype=np.int8),
                                   np.zeros(db.n_test, dtype=np.int8)))
            subjs = db.train_list + db.train_list + \
                db.valid_list + db.test_list
        else:
            sess = np.concatenate((np.zeros(db.n_train, dtype=np.int8),
                                   np.ones(db.n_train, dtype=np.int8)))
            subjs = db.train_list + db.train_list
        ######################################################################
        # 3. Iterate trough events and load data for created lists of
        #    subjects and sessions
        ######################################################################
        for ev_idx, ev in enumerate([1, 2, 4, 5, 6]):
            t_data, t_gt = self.load_data(exp_out_path, subjs, sess, [ev])
            t_data_ev_path = os.path.join(train_data_path, 'event_' + str(ev))
            if not os.path.exists(t_data_ev_path):
                os.makedirs(t_data_ev_path)
            np.save(os.path.join(t_data_ev_path, 'train_data.npy'), t_data)
        ######################################################################
        # 4. Define validation data path
        ######################################################################
        valid_data_path = \
            os.path.join(exp_out_path, 
                         'valid(subj_aware={0}, {1}, L={2}, lambda_={3})'.\
                            format(self.subj_aware, self.spat_type, 
                                   self.L, self.lambda_))
        ######################################################################
        # 5. Define validation subject and session lists
        ######################################################################
        if self.subj_aware:
            sess = np.ones(db.n_valid,  dtype=np.int8)
            subjs = db.valid_list
        else:
            sess = np.concatenate((np.zeros(db.n_valid, dtype=np.int8),
                                   np.ones(db.n_valid, dtype=np.int8)))
            subjs = db.valid_list + db.valid_list
        ######################################################################
        # 6. Iterate trough events and load data for created lists of
        #    subjects and sessions
        ######################################################################
        for ev_idx, ev in enumerate([1, 2, 4, 5, 6]):
            v_data, v_gt = self.load_data(exp_out_path, subjs, sess, [ev])
            v_data_ev_path = os.path.join(valid_data_path, 'event_' + str(ev))
            if not os.path.exists(v_data_ev_path):
                os.makedirs(v_data_ev_path)
            np.save(os.path.join(v_data_ev_path, 'valid_data.npy'), v_data)
        ######################################################################
        # 7. Set up done flag
        ######################################################################
        with open(os.path.join(train_data_path, 'done'), 'a+') as f:
            f.close()

    def load_prepared_data(self, db, exp_out_path, subset='train', 
                           n_t_samples=1280):
        """Loads previously prepared merged data."""
        ######################################################################
        # 1. Define data path
        ######################################################################
        data_path = \
            os.path.join(exp_out_path, 
                         '{0}(subj_aware={1}, {2}, L={3}, lambda_={4})'.\
                         format(subset, self.subj_aware, self.spat_type, 
                                self.L, self.lambda_))
        ######################################################################
        # 2. Count number of trials for memory allocation
        ######################################################################
        c = 0
        for ev_idx, ev in enumerate([1, 2, 4, 5, 6]):
            ev_path = os.path.join(data_path, 'event_{0}'.format(ev))
            ev_data = np.load(os.path.join(ev_path, subset + '_data.npy'))
            if subset == 'train':
                c += n_t_samples
            else:
                c += ev_data.shape[0]
            n = ev_data.shape[1]
            t = ev_data.shape[2]
        ######################################################################
        # 3. Load prepared data
        ######################################################################
        data = np.zeros((c, n, t), dtype=np.float32)
        gt = np.zeros((c, 5), dtype=np.int8)
        c = 0
        for ev_idx, ev in enumerate([1, 2, 4, 5, 6]):
            ev_path = os.path.join(data_path, 'event_{0}'.format(ev))
            ev_data = np.load(os.path.join(ev_path, subset+'_data.npy'))
            if subset == 'train':
                n_ev = ev_data.shape[0]
                rnd_s = np.random.choice(n_ev, n_t_samples, replace=False)
                data[c:c + n_t_samples, :, :] = ev_data[rnd_s, :, :]
                gt[c:c + n_t_samples, ev_idx] = 1
                c += n_t_samples
            else:
                n_ev = ev_data.shape[0]
                data[c:c + n_ev, :, :] = ev_data
                gt[c:c + n_ev, ev_idx] = 1
                c += n_ev
        ######################################################################
        return data, gt

    def load_data(self, exp_out_path, subjects, sessions, evs=[1, 2, 4, 5, 6]):
        ######################################################################
        # 1. Count number of trials for memory allocation
        ######################################################################
        n_subjects = len(subjects)
        c = 0
        for s_idx in range(n_subjects):
            for ev in evs:
                pp_out_path = os.path.join(exp_out_path,
                                           'subject_' + subjects[s_idx],
                                           'session_' + str(sessions[s_idx]),
                                           'event_' + str(ev))
                info_path = os.path.join(pp_out_path, 'data_info')
                with open(info_path, 'r') as f_inf:
                    line = str.split(f_inf.readline())
                    n = int(line[1])
                    c += int(line[0])
        ######################################################################
        # 2. Load preprocessed data
        ######################################################################
        if self.spat_type == 'sph':
            n = np.sum([2 * l + 1 for l in range(0, self.L+1)])
        data = np.zeros((c, n, self.n_temp), dtype=np.float32)
        gt = np.zeros((c, 5), dtype=np.int8)
        c = 0
        for s_idx in range(n_subjects):
            for ev_idx, ev in enumerate(evs):
                pp_out_path = os.path.join(exp_out_path,
                                           'subject_' + subjects[s_idx],
                                           'session_' + str(sessions[s_idx]),
                                           'event_' + str(ev))

                data_s = np.load(os.path.join(pp_out_path, 'data.npy')) * 5
                if self.spat_type == 'sph':
                    coords_path = os.path.join(pp_out_path, 'norm_coords.npy')
                    coords_cart = np.load(coords_path)
                    coords_s2 = convert_cart_to_s2(coords_cart)
                    Y_inv = lms_laplace_beltrami_sh_inv(coords_s2, 
                                                        L=self.L, 
                                                        lambda_=self.lambda_)
                    data_s = np.einsum('lp,npt->nlt', Y_inv, data_s)

                n_trials = data_s.shape[0]
                gt[c:c + n_trials, ev_idx] = 1
                data[c:c + n_trials, :, :] = data_s[:, :, :]
                c += n_trials
        ######################################################################
        return data, gt

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
