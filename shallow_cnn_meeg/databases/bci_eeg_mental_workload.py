
import os
from .. import Database

import numpy as np


class Database_BCI_EEG_MentalWorkload(Database):

    def __init__(self, db_path=None):

        self.db_path = db_path
        pass

    def set_db_path(self, db_path):

        self.db_path = db_path

    def get_subjects_list(self):

        #subjects = sorted(os.listdir(self.db_path))
        subjects_mw = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']

        self.subjects_list = subjects_mw

    def split_subjects(self, seed=1):

        n_subjects = len(self.subjects_list)

        idx = np.arange(0, n_subjects)

        np.random.seed(seed)
        np.random.shuffle(idx)

        self.train_list = [self.subjects_list[i] for i in idx[0:9]]
        self.n_train = len(self.train_list)
        self.valid_list = [self.subjects_list[i] for i in idx[9:12]]
        self.n_valid = len(self.valid_list)
        self.test_list = [self.subjects_list[i] for i in idx[12:]]
        self.n_test = len(self.test_list)

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
