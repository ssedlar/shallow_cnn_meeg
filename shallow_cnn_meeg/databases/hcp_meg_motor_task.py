
import os
from .. import Database

import numpy as np


class Database_HCP_MEG_MotorTask(Database):

    def __init__(self, db_path=None):

        self.db_path = db_path
        pass

    def set_db_path(self, db_path):

        self.db_path = db_path

    def get_subjects_list(self):

        subjects_mt = ['104012', '105923', '106521', '108323', '109123', '113922', '116726',
                       '125525', '133019', '140117', '151526', '153732', '156334', '162026',
                       '162935', '164636', '169040', '175237', '177746', '185442', '189349',
                       '191033', '191437', '191841', '192641', '198653', '200109', '204521',
                       '205119', '212318', '212823', '221319', '250427', '255639', '257845',
                       '283543', '287248', '293748', '353740', '358144', '406836', '500222',
                       '559053', '568963', '581450', '599671', '601127', '660951', '662551',
                       '667056', '679770', '680957', '706040', '707749', '725751', '735148',
                       '783462', '814649', '891667', '898176', '912447']
        self.subjects_list = subjects_mt

    def split_subjects(self, seed=1):

        n_subjects = len(self.subjects_list)

        idx = np.arange(0, n_subjects)

        np.random.seed(seed)
        np.random.shuffle(idx)

        self.train_list = [self.subjects_list[i] for i in idx[0:20]]
        self.n_train = len(self.train_list)
        self.valid_list = [self.subjects_list[i] for i in idx[20:30]]
        self.n_valid = len(self.valid_list)
        self.test_list = [self.subjects_list[i] for i in idx[30:]]
        self.n_test = len(self.test_list)

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
