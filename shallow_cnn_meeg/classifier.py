import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import Model

class Classifier(Model):

    def __init__(self, n_classes=3, n_epochs=100, nt_batch=64, nv_batch=256, 
                 lr=0.0005, exp_name='exp_1'):
        super(Classifier, self).__init__()

        self.n_classes = n_classes

        self.n_epochs = n_epochs
        self.lr = lr
        self.nt_batch = nt_batch
        self.nv_batch = nv_batch

        self.exp_name = exp_name

    @tf.function
    def get_outputs(self, data_batch):
        return self(data_batch)

    @tf.function
    def train_step(self, data_batch, data_labs):
        with tf.GradientTape() as tf_tape:
            estims= self(data_batch, training=True)
            loss = self.loss(data_labs, estims)

        grads = tf_tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(grads, self.trainable_variables))
        return loss

    @tf.function
    def valid_step(self, data_batch, data_labs):
        estims = self(data_batch, training=False)
        error = self.loss(data_labs, estims)
        return error, estims

    def save_model_weights(self, output_m_path):
        """Save model weights."""
        """
            Arguments:
                output_m_path (str): output model path
        """
        if not os.path.exists(output_m_path):
            os.makedirs(output_m_path)

        for v_idx, v in enumerate(self.trainable_variables):
            var_name = 'var_' + str(v_idx) + '.npy'
            var_path = os.path.join(output_m_path, var_name)
            np.save(var_path, v.numpy())

    def train_and_valid(self, db, prep, exp_out_path, results_path, seed=1):

        r_exp_path = os.path.join(results_path,  self.name)
        if not os.path.exists(r_exp_path):
            os.makedirs(r_exp_path)

        v_data, v_gt = prep.load_prepared_data(db, exp_out_path, subset='valid', seed=seed)
        v_data_tf = tf.data.Dataset.from_tensor_slices((v_data, v_gt)).\
            batch(self.nv_batch)
        n_samples_v = v_data.shape[0]

        v_clf_acc_np = np.zeros(self.n_epochs + 1)
        v_clf_acc_txt_path = os.path.join(r_exp_path, 'valid_clf_acc.txt')

        t_data, t_gt = prep.load_prepared_data(db, exp_out_path, subset='train', seed=seed)
        n_samples_t = t_data.shape[0]
        t_data_tf = tf.data.Dataset.from_tensor_slices((t_data, t_gt)).\
            shuffle(n_samples_t).batch(self.nt_batch)

        v_max =0
        for e in range(1, self.n_epochs+1):

            for t_idx, (t_batch, t_labs) in enumerate(t_data_tf):
                loss = self.train_step(t_batch, t_labs)

            v_clf = np.zeros(n_samples_v)
            for v_idx, (v_batch, v_labs) in enumerate(v_data_tf):
                v_loss, v_estims = self.valid_step(v_batch, v_labs)
                v_s = v_idx * self.nv_batch
                v_e = (v_idx + 1) * self.nv_batch
                v_clf[v_s:v_e] = \
                    np.argmax(v_labs, axis=1) == np.argmax(v_estims, axis=1)

            v_clf_acc_np[e] = np.mean(v_clf)

            with open(v_clf_acc_txt_path, 'a+') as f_err:
                f_err.write('%s\n' % (v_clf_acc_np[e]))

            if e > 10:

                v_clf_acc_last_6 = v_clf_acc_np[e - 6 + 1: e + 1]
                mean_3_first = np.mean(v_clf_acc_last_6[0:3])
                mean_3_last = np.mean(v_clf_acc_last_6[3:])

                if (mean_3_last - mean_3_first) < 1.e-4:
                    lr_c = self.optimizer.lr.read_value()
                    self.optimizer.lr.assign(lr_c * 0.9)
                    print(self.optimizer.lr.read_value())
            
            if ((e % 100 == 0 and v_max < v_clf_acc_np[e]) or e == self.n_epochs) and e:
                v_max = v_clf_acc_np[e]
                self.test(db, prep, e, exp_out_path, results_path)
                np.save(os.path.join(r_exp_path, 'valid_clf_acc.npy'), v_clf_acc_np)
                out_m_path = os.path.join(r_exp_path, 'model_{0}')
                self.save_model_weights(out_m_path.format(e))
            
    def test(self, db, prep, e, exp_out_path, results_path):

        results_out_path = os.path.join(results_path, self.name)
        if not os.path.exists(results_out_path):
            os.makedirs(results_out_path)

        output_test_path = os.path.join(results_out_path, 'test_{0}'.format(e))
        if not os.path.exists(output_test_path):
            os.makedirs(output_test_path)
        if prep.subj_aware:
            sessions = [1]
        else:
            sessions = [0, 1]
        for t_subj in db.test_list:
            for sess in sessions:
                t_data, t_gt = prep.load_data(exp_out_path, [t_subj], [sess])
                t_data_tf = \
                    tf.data.Dataset.from_tensor_slices((t_data, t_gt)).\
                        batch(self.nv_batch)
                t_estims = np.zeros_like(t_gt).astype(np.float32)

                for t_idx, (t_batch, t_labs) in enumerate(t_data_tf):
                    t_loss, t_est = self.valid_step(t_batch, t_labs)
                    t_s = t_idx * self.nv_batch
                    t_e = (t_idx + 1) * self.nv_batch
                    t_estims[t_s:t_e, :] = t_est

                output_spec_test_path = os.path.join(output_test_path,
                                                     'subject_{0}',
                                                     'session_{1}').format(t_subj, sess)

                if not os.path.exists(output_spec_test_path):
                    os.makedirs(output_spec_test_path)

                np.save(os.path.join(output_spec_test_path, 'class_estims.npy'), t_estims)
                np.save(os.path.join(output_spec_test_path, 'class_gt.npy'), t_gt)


    def name(self):
        """Return object's name."""
        raise NotImplementedError()
